
import bundle from "./bundle.js";
import minify from "./minify.js";
import { eS62ES5 } from "./convert.js";
import fs from "fs";

export default class Bundler {
  /**
   * @param {Object<string,BMCConfig|BMCConfig[]>} taskList 
   */
  constructor(taskList) {
    this._taskList = taskList;
  }
  listTasks() {
    return Object.keys(this._taskList);
  }
  runTask(taskName) {
    const task = this._taskList[taskName];
    if (!task) {
      process.stdout.write(`名稱為"${taskName}"之 task 不存在\n`);
      return;
    }
    else {
      _runTask(task, taskName);
    }
  }
}

/** @param {BMCConfig} task */
function _runTask(task) {
  const { bundle: bundleConf, minify : minifyConf = true, convert = true, output } = task;

  let resultPromise;
  if (minifyConf === true) {
    resultPromise = bundle(bundleConf.filePath, bundleConf.varName) // bundle
      .then(code => convert ? eS62ES5(code) : code) // es6 to es5
      .then(code => minify(code)); // minify
  }
  else if (minifyConf === false) {
    resultPromise = bundle(bundleConf.filePath, bundleConf.varName) // bundle
      .then(code => convert ? eS62ES5(code) : code); // es6 to es5
  }
  else {
    resultPromise = bundle(bundleConf.filePath, bundleConf.varName) // bundle
    .then(code => convert ? eS62ES5(code) : code) // es6 to es5
    .then(code => minify(code, minifyConf)) // minify
  }
  resultPromise.then(code => fs.writeFile(output.filePath, code, (err) => {
    if (err) process.stdout.write(`名稱為"${name}"之task檔案儲存失敗\n`);
  }));
}

/**
 * @typedef {Object} BMCConfig
 * @property {{filePath:string,varName:string}} bundle
 * @property {true|false|{privateProps?:boolean}} [minify=true]
 * @property {boolean} [convert=true]
 * @property {{filePath:string}} output
 */
