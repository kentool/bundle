import UglifyJS from "uglify-es";

/**
 * 
 * @param {string} code 
 * @param {{privateProps:boolean}} [options={privateProps:false}] 
 */
export default function minify(code, options = {}) {
  let { privateProps = false } = options;
  let minifyOptions = {};
  if (privateProps)
    minifyOptions.mangle = {
      properties: {
        regex: /^_p_/
      }
    };

  return UglifyJS.minify(code, minifyOptions).code;
  // return UglifyJS.minify(code, {
  //   mangle: {
  //     properties: {
  //       regex: /^_/
  //     }
  //   },
  //   output: {
  //     comments: /@public/
  //   }
  // }).code;
}

//// 底下為測試程式
// let code = `class AAA {
//   constructor(param1, param2) {
//     this._prop1 = param1;
//     this._prop2 = param2;
//   }
//   /**@public
//    * 屬性1
//    */
//   get prop1() { return this._prop1; }
//   /**@public 屬性2 */
//   get prop2() { return this._prop2; }
//   /**
//    * 測試方法
//    */
//   _test() { console.log("test"); }
//   /** @public
//    * 方法
//    */
//   ttt() { return this._prop1 + this._prop2; }
// }

// let aaa = new AAA(1, -1);
// console.log(aaa.prop1);
// console.log(aaa.prop2);
// console.log(aaa._prop1);
// console.log(aaa._prop2);
// aaa._test();
// aaa.ttt();
// export default aaa;`;
// let _code = minify(code);
// console.log("");