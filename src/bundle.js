import { rollup } from "rollup";

/**
 * 
 * @param {string} filePath 
 * @param {string} varName 
 */
export default function bundle(filePath, varName) {
  return rollup({
    input: filePath
  }).then(result => result.generate({
    format: "iife",
    name: varName
  })).then(result => result.output[0].code);
}
