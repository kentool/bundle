import { transform } from "@babel/core";
/**
 * 
 * @param {string} code 
 */
export function eS62ES5(code) {
  return transform(code, {
    presets: ["@babel/env"],
    plugins: [
      ["@babel/plugin-proposal-class-properties", { "loose": true }]
    ]
  }).code;
}